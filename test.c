#include <stdio.h>
#include "prntf/ft_printf.h"
#include <limits.h>
#include <float.h>
#include <time.h>
#include <inttypes.h>
#include <math.h>
#include <wchar.h>
#include <locale.h>

typedef union u_abc
{
	LD a;
	U_CHAR ar_a[sizeof(LD)];
	short	ar_short[sizeof(LD)];
} s_abc;

int main()
{
	double z1 = 1.723432E308;
	long double z2 = 1.25E-3L;
	int k = 9;

	double start, end, start1, end1;
	LD f = 3.14;
	LD v = 3.12345;
	int *pk = &k;
	s_abc l_a;
	l_a.a = 12.0;
	
	// ft_printbits(l_a.ar_a[8], 8);
	// ft_printbits(l_a.ar_a[9], 8);
	// ft_printbits(l_a.ar_short[4], 16);
	l_a.ar_short[4] = 16383;
	// ft_printbits(l_a.ar_short[4], 16);
	// setlocale(LC_ALL, "");
	// start = clock();
	short a11  = -25;
	int okok[3] = {937, 937, 0};
	int abc = printf( "%1$2.3Lf %2$-43.26ls, %4$12.12d, %3$31.31f",123.123L, L"こんにちは、私はprintf単体テストです",564645.6543424432 , 4321111);
	// ft_putchar('\n');
	int bca = ft_printf( "%1$2.3Lf !/%2$-43.26ls!/, %4$12.0d, %3$31.30f",123.123L, L"こんにちは、私はprintf単体テストです",564645.6543424432 , 4321111);
	printf("\n");
	//  bca = ft_printf( "%2$12.0d, %1$31.30f", 564645.6543424432 , 4321111);
	// int abc = printf("%.% %.% %.");
	// end = clock();
	// printf("");
	// start1 = clock();
	// int abc = printf("%");
	// int bca = ft_printf("^.^/%ls^.^/", L"こんにちは、私はprintf単体テストです");
	// end1 = clock();
	// int ok[2] = {937, 937, 0};
	// printf("\n");
	// printf("%.3ls", ok);
	// int bca = ft_printf("%-32ls", L"いいえ");
	// int bca = ft_printf("%s", "abc");
	// printf("%d, %d\n", abc, 3);
	printf("\n%d %d\n", abc, bca);
	// printf("p%.10f, ft_p%.10f\n", (end - start)/CLOCKS_PER_SEC,
	// (end1 - start1) / CLOCKS_PER_SEC);
}